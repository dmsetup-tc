CC = gcc
LDFLAGS = -lgcrypt
CFLAGS = -O3
BIN = dmsetup-tc

.PHONY: all clean

all: $(BIN)

clean:
	rm -f *.o $(BIN)

$(BIN): main.o crc32.o xts.o pkcs5.o
	$(CC) -o $@ $^ $(LDFLAGS)

%.o: %.c
	$(CC) $(CFLAGS) -c $<

dmtc_version.h_dummy:
	tools/get-version

main.o: main.c dmtc.h dmtc_version.h_dummy crc32.h xts.h pkcs5.h

