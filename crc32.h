#ifndef CRC32_H
#define CRC32_H

/* Compute and return a 32 bit CRC of the buffer. */
unsigned long crc32(const unsigned char *buf, unsigned int len);

#endif /* CRC32_H */
