#ifndef DMTC_H
#define DMTC_H

struct sized_data {
	size_t size;
	unsigned char *data;
};

enum validate_result {
	VALID_HEADER,
	INVALID_SIGNATURE,
	KEY_CHECKSUM_MISMATCH,
	HEADER_CHECKSUM_MISMATCH,
	NOT_SYSTEM_ENCRYPTION,
	PARTIALLY_ENCRYPTED
};

typedef struct sized_data sdata;
typedef enum validate_result valid_res;

#endif /* DMTC_H */
