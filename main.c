/* main.c: dmsetup-tc main program
 *
 * Copyright (C) 2008 Jan Krueger <jk@jk.gs>
 *
 *   This file is part of dmsetup-tc.
 *
 *   dmsetup-tc is free software: you can redistribute it and/or
 *   modify it under the terms of the GNU General Public License as
 *   published by the Free Software Foundation, either version 3 of the
 *   License, or (at your option) any later version.
 *
 *   dmsetup-tc is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License along
 *   with dmsetup-tc.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <errno.h>
#include <inttypes.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>

#ifdef DEBUG
void trace(const char *msg, ...)
{
	va_list ap; va_start(ap, msg);
	vfprintf(stderr, msg, ap);
	va_end(ap);
}
#else
void trace(const char *msg, ...) {}
#endif

#include "dmtc.h"
#include "dmtc_version.h"
#include "xts.h"
#include "pkcs5.h"
#include "crc32.h"
#include <gcrypt.h>

/* Disk interface {{{ ***************************************************** */

sdata *read_header(const char *file)
{
	unsigned char *buf;
	sdata *vh;
	int verr;
	struct stat sb;

	if (stat(file, &sb) == -1)
		return NULL;

	FILE *f = fopen(file, "r");
	if (!f) return NULL;

	switch (sb.st_mode & S_IFMT) {
	case S_IFBLK:
		/* Probably a disk device; seek to TC header */
		if (-1 == fseek(f, 62*512, SEEK_SET)) goto err_nofree;
		break;
	case S_IFREG:
		/* Probably a header file, so don't seek */
		break;
	default:
		fclose(f);
		return NULL;
	}

	buf = malloc(512);
	if (!buf) goto err_nofree;

	if (1 != fread(buf, 512, 1, f)) goto err;

	fclose(f);
	vh = malloc(sizeof (sdata));
	vh->data = buf;
	vh->size = 512;
	return vh;

err:
	free(buf);
err_nofree:
	verr = errno;
	fclose(f);
	errno = verr;
	return NULL;
}

static size_t get_part_info(const char *part, const char *type)
{
	char buf[512];
	char part_name[16];
	FILE *f;
	size_t val;
	int verr;

	if (1 != sscanf(part, "/dev/%15s", part_name)) return 0;
	snprintf(buf, 512, "/sys/class/block/%s/%s", part_name, type);

	f = fopen(buf, "r");
	if (!f) return 0;

	if (1 != fscanf(f, "%zu", &val)) {
		verr = errno;
		fclose(f);
		errno = verr;
		return 0;
	}

	fclose(f);
	return val;
}

size_t get_part_start_sector(const char *part)
{
	return get_part_info(part, "start");
}

size_t get_part_num_sectors(const char *part)
{
	return get_part_info(part, "size");
}

/* }}} ******************************************************************** */

/* Data extraction interface {{{ ****************************************** */

sdata *get_header_salt(sdata *header)
{
	sdata *salt = malloc(sizeof (sdata));
	salt->data = header->data;
	salt->size = 64;
	return salt;
}

unsigned int boswap(int x)
{
	unsigned char *c = (unsigned char *) &x;
	return (c[0] << 24) + (c[1] << 16) + (c[2] << 8) + c[3];
}

int validate_header(sdata *header)
{
	unsigned char *data = header->data;
	int i = header->size - 256;

	trace("  * Validating header signature\n");
	if (data[0] != 'T' || data[1] != 'R' || data[2] != 'U' ||
			data[3] != 'E') return INVALID_SIGNATURE;

	trace("  * Validating header checksum\n");
	if (boswap(((uint32_t *) data)[47]) !=
			crc32(data, 188))
		return HEADER_CHECKSUM_MISMATCH;

	trace("  * Validating checksum of master keys\n");
	if (boswap(((uint32_t *) data)[2]) !=
			crc32(&data[i], 256))
		return KEY_CHECKSUM_MISMATCH;

	trace("  * Making sure we're dealing with system encryption\n");
	if (!(data[63] & 1)) return NOT_SYSTEM_ENCRYPTION;

	trace("  * Making sure we're not dealing with partial encryption\n");
	if (memcmp(&data[36], &data[52], 8)) return PARTIALLY_ENCRYPTED;

	return VALID_HEADER;
}

sdata *get_header_keys(sdata *header)
{
	sdata *res = malloc(sizeof (sdata));

	res->data = header->data + 192;
	res->size = 256;

	return res;
}

/* }}} ******************************************************************** */

/* Crypt interface {{{ **************************************************** */

void crypt_init()
{
	gcry_check_version(NULL);
	gcry_control(GCRYCTL_INIT_SECMEM, 1048576);
}

sdata *crypt_derive_key(sdata *pass, sdata *salt)
{
	sdata *res;
	unsigned char *vkey;

	vkey = malloc(64);
	res = malloc(sizeof (sdata));

	if (0 != gcry_pbkdf2(GCRY_MD_RMD160,
			(const char *) (pass->data), pass->size,
			(const char *) (salt->data), salt->size,
			1000,
			64,
			(char *) vkey)) {
		free(vkey);
		free(res);
		return NULL;
	}

	res->data = vkey;
	res->size = 64;
	return res;
}

sdata *crypt_decrypt_header(sdata *header, sdata *key)
{
	sdata *res;
	unsigned char *data = header->data + 64;

	if (0 != aes_xts_decrypt(key->data, NULL, 28, 0, data)) return NULL;

	res = malloc(sizeof (sdata));
	res->data = data;
	res->size = header->size - 64;
	return res;
}

/* }}} ******************************************************************** */

/* Auxiliary CLI functions {{{ ******************************************** */

void die_banner()
{
	fputs(
"dmsetup-tc version " DMTC_VERSION "\n"
"Copyright (c) <year range here, who cares> Jan Krueger <jk@jk.gs>\n"
"\n"
"    This program is free software: you can redistribute it and/or modify\n"
"    it under the terms of the GNU General Public License as published by\n"
"    the Free Software Foundation, either version 3 of the License, or\n"
"    (at your option) any later version.\n"
"\n"
"    This program is distributed in the hope that it will be useful,\n"
"    but WITHOUT ANY WARRANTY; without even the implied warranty of\n"
"    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\n"
"    GNU General Public License for more details.\n"
"\n"
"    You should have received a copy of the GNU General Public License\n"
"    along with this program.  If not, see <http://www.gnu.org/licenses/>.\n",
	stderr);
	exit(0);
}

void die_error(char *err)
{
	if (!err) {
		perror("dmsetup-tc: fatal error");
	} else {
		fprintf(stderr, "dmsetup-tc: fatal error: %s\n", err);
	}
	exit(2);
}

void die_root()
{
	puts("Fatal: dmsetup-tc needs to run as root");
	exit(99);
}

void die_syntax()
{
	puts(
"Syntax: dmsetup-tc [--stdin] <disk|header file> <partition>\n"
"  --stdin     Read passphrase from stdin rather than tty (do not prompt)\n"
"\n"
"Example: dmsetup-tc /dev/sda /dev/sda3 | dmsetup create myvolume\n"
"         dmsetup-tc headerfile /dev/sda3 | ...\n"
	);
	exit(64);
}

/* }}} ******************************************************************** */

int main(int argc, char *argv[])
{
	int ofs = 1;
	int from_stdin = 0;
	char *header_file, *part_dev;
	sdata pass;
	sdata *vh;
	sdata *dh;
	sdata *vkey;
	sdata *hkey;
	size_t size, start;
	char hkey_hex[129];
	valid_res val;
	int i;

	if (argc > 1 && !strcmp(argv[1], "--version"))
		die_banner();
	if (argc == 1 || !strcmp(argv[1], "--help"))
		die_syntax();

	if (geteuid())
		die_root();

	/* Parse options */
	while (ofs < argc) {
		if (!strcmp(argv[ofs], "--stdin")) {
			from_stdin = 1;
			ofs++;
			continue;
		}
		break;
	}
	if ((argc - ofs) < 2) die_syntax();

	header_file = argv[ofs];
	part_dev = argv[ofs+1];

	pass.data = getpass(from_stdin ? "" :
		"Enter passphrase for encrypted volume: ");
	pass.size = strlen(pass.data);
	if (0 != mlock(pass.data, pass.size)) die_error(NULL);

	trace("Loading header from %s...\n", header_file);
	vh = read_header(header_file);
	if (!vh) die_error(NULL);

	crypt_init();

	trace("Deriving header key...\n");
	vkey = crypt_derive_key(&pass, get_header_salt(vh));
	if (!vkey) die_error(NULL);

	trace("Decrypting header...\n");
	dh = crypt_decrypt_header(vh, vkey);
	if (!dh) {
		char buf[1024];
		snprintf(buf, 1024, "%s/%s", aes_xts_err, aes_xts_errsrc);
		die_error(buf);
	}

	trace("Validating header...\n");
	val = validate_header(dh);
	switch (val) {
	case INVALID_SIGNATURE:
		fputs(
"Could not decrypt the volume. You probably entered a wrong password.\n",
			stderr);
		break;
	case KEY_CHECKSUM_MISMATCH:
		fputs(
"The master key in your volume header seems to be corrupted. I can't\n"
"really see this happening on its own, except maybe if your RAM is broken.\n"
"Have it checked and try restoring the header from your rescue CD.\n",
			stderr);
		break;
	case HEADER_CHECKSUM_MISMATCH:
		fputs(
"Your volume header is corrupted. I suspect faulty RAM. Have it checked\n"
"(and possibly fixed) and restore the header from your rescue CD.\n",
			stderr);
		break;
	case NOT_SYSTEM_ENCRYPTION:
		fputs(
"This volume is not using system drive/partition encryption. This program\n"
"only supports volumes within the scope of system encryption. Please use\n"
"the official TrueCrypt(R) program for accessing this volume.\n",
			stderr);
		break;
	case PARTIALLY_ENCRYPTED:
		fputs(
"System encryption is still in progress. We can't really support partially\n"
"encrypted volumes under Linux without a lot of effort, so please have the\n"
"official TrueCrypt(R) program for Windows finish encrypting the volume\n"
"before you use this program.\n",
			stderr);
		break;
	}
	if (val != VALID_HEADER) exit(1);

	hkey = get_header_keys(dh);
	size = get_part_num_sectors(part_dev);
	start = get_part_start_sector(part_dev);

	for (i = 0; i < 16; i++) {
		sprintf(&hkey_hex[i*8], "%08x", boswap(((unsigned int *) hkey->data)[i]));
	}

	printf("0 %zu crypt aes-xts-plain %s %zu %s 0", size, hkey_hex,
		start, part_dev);
	exit(0);
}
