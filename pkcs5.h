#ifndef PKCS5_H
#define PKCS5_H

#include <gcrypt.h>

gcry_err_code_t gcry_pbkdf2(int PRF, const char *P, size_t Plen, const char *S,
	size_t Slen, unsigned int c, unsigned int dkLen, char *DK);

#endif
