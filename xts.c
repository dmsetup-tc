/*
 * xts.c: AES-XTS implementation
 *
 * Copyright (C) 2008 Jan Krueger <jk@jk.gs>
 *
 *   This file is part of dmsetup-tc.
 *
 *   dmsetup-tc is free software: you can redistribute it and/or
 *   modify it under the terms of the GNU General Public License as
 *   published by the Free Software Foundation, either version 3 of the
 *   License, or (at your option) any later version.
 *
 *   dmsetup-tc is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License along
 *   with dmsetup-tc.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <errno.h>
#include <gcrypt.h>
#include <string.h>

#include "xts.h"

/*
 * This is a partial implementation of AES-XTS (using libgcrypt for
 * AES-256). Care was taken to keep the code readable. The algorithm is
 * explained in various comments.
 *
 * GLOSSARY
 * ========
 *
 * Block
 *     "Chain-encrypted" blocks of data inside a _data unit_.
 *
 * Data unit
 *     Separately encrypted units of data, e.g. disk sectors. Each data unit
 *     uses a different _tweak_ for decryption to prevent a variety of
 *     attacks.
 *
 * Tweak
 *     Usually something like a sector number. More generally, a series of
 *     positive numbers starting at a certain point and increasing by one
 *     for each new data unit.
 *
 * CAVEATS
 * =======
 *
 * This code only works on little-endian systems. Fix xts_shift if you care.
 *
 * Ciphertext stealing is not supported, i.e. the data unit size better be
 * divisible by the block size.
 *
 * For simplicity, we limit the tweak to 32 bit values. This is sufficient
 * for disk encryption for disk sectors below 2 terabytes.
 *
 * We always assume we're starting to decrypt at the first block, i.e. block
 * index = 0.
 */

#define XTS_BLOCKSIZE 16

typedef unsigned int *xts_block32;

const char *aes_xts_err;
const char *aes_xts_errsrc;

/*
 * Performs one left shift of the encrypted tweak. We call this after each
 * round of block decryption so that the value gets shifted further each
 * time.
 */
static void xts_shift(xts_block32 b)
{
	unsigned int x = (b[3] & 0x80000000) ? 135 : 0;
	b[3] <<= 1; if (b[2] & 0x80000000) b[3] |= 1;
	b[2] <<= 1; if (b[1] & 0x80000000) b[2] |= 1;
	b[1] <<= 1; if (b[0] & 0x80000000) b[1] |= 1;
	b[0] <<= 1; b[0] ^= x;
}
static void xts_xor(xts_block32 dst, xts_block32 src)
{
	dst[3] ^= src[3];
	dst[2] ^= src[2];
	dst[1] ^= src[1];
	dst[0] ^= src[0];
}

/*
 * Decrypts a series of blocks. Works in-place if so desired by setting
 * cipher to NULL (in which case existing ciphertext will be taken from
 * plain). Of course, overlapping buffers are evil scary verboten). Be sure
 * to initialize libgcrypt beforehand, too (and have it init secure memory).
 */
int aes_xts_decrypt(unsigned char *key, unsigned char *cipher, int numblocks,
	unsigned int tweak, unsigned char *plain)
{
	char buf[16];
	xts_block32 etweak;
	gcry_cipher_hd_t hd;
	gcry_error_t err;
	int i;

	/*
	 * XTS actually uses twice the key size of the underlying cipher.
	 * The second half of the key is used to encrypt the tweak.
	 */
	char *key2 = (char *) &key[32];

	etweak = (xts_block32) buf;
	memset(etweak, 0, 16);
	etweak[0] = tweak;

#define _xts_checkerr(c, x) {err = c;\
	if (0 != gcry_err_code(err)) {\
		errno = gcry_err_code_to_errno(err);\
		aes_xts_err = gcry_strerror(err);\
		aes_xts_errsrc = gcry_strsource(err);\
		if (x) gcry_cipher_close(x);\
		return -1;\
	}}

	_xts_checkerr(gcry_cipher_open(&hd, GCRY_CIPHER_AES256,
		GCRY_CIPHER_MODE_ECB, GCRY_CIPHER_SECURE), 0);
	_xts_checkerr(gcry_cipher_setkey(hd, key2, 32), hd);
	_xts_checkerr(gcry_cipher_encrypt(hd, etweak, XTS_BLOCKSIZE, NULL,
		0), hd);

	/* For the rest we use the main key, i.e. the first half. */
	_xts_checkerr(gcry_cipher_setkey(hd, key, 32), hd);

	for (i = 0; i < numblocks; i++) {
		if (cipher) {
			xts_xor((xts_block32) cipher, etweak);
			_xts_checkerr(gcry_cipher_decrypt(hd, plain,
				XTS_BLOCKSIZE, cipher, XTS_BLOCKSIZE), hd);
			cipher += XTS_BLOCKSIZE;
		} else {
			xts_xor((xts_block32) plain, etweak);
			_xts_checkerr(gcry_cipher_decrypt(hd, plain,
				XTS_BLOCKSIZE, NULL, 0), hd);
		}
		xts_xor((xts_block32) plain, etweak);
		plain += XTS_BLOCKSIZE;
		xts_shift(etweak);
	}

	gcry_cipher_close(hd);
	return 0;
}
