#ifndef XTS_H
#define XTS_H

extern const char *aes_xts_err;
extern const char *aes_xts_errsrc;

int aes_xts_decrypt(unsigned char *key, unsigned char *cipher, int numblocks,
	unsigned int tweak, unsigned char *plain);

#endif
